import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import _ from 'lodash'

import { apiLogin, apiMe } from 'services/auth'

export const loginAction = createAsyncThunk('auth/login', async ({ username, password }) => {
  const loginResult = await apiLogin({ username, password })
  if (loginResult.status === 'error') {
    return {
      status: 'error',
      errorMessage: 'User not found',
      data: {},
    }
  }

  const token = loginResult.data.token
  const meResult = await apiMe(token)
  if (_.isEmpty(meResult.data)) {
    return {
      status: 'error',
      errorMessage: 'User not found',
      data: {},
    }
  }

  return {
    status: 'success',
    errorMessage: null,
    data: {
      token,
      user: meResult.data,
    },
  }
})

export const logoutAction = createAsyncThunk('auth/logout', async () => {
  return true
})

export const meAction = createAsyncThunk('auth/me', async () => {
  const token = window.localStorage.getItem('token')
  if (!token) {
    return {
      status: 'error',
      errorMessage: 'Token not found',
    }
  }

  const meResult = await apiMe(token)
  if (_.isEmpty(meResult.data)) {
    return {
      status: 'error',
      errorMessage: 'User not found',
      data: {},
    }
  }

  return {
    status: 'success',
    errorMessage: null,
    data: {
      user: meResult.data,
    },
  }
})

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoggingIn: false,
    isLogin: false,
    isLoginChecked: false,
    token: null,
    data: {},
  },
  reducers: {},
  extraReducers: {
    [loginAction.pending]: (state, action) => {
      state.isLoggingIn = true
    },
    [loginAction.fulfilled]: (state, action) => {
      const { status, data } = action.payload
      const isLogin = status === 'success'
      state.isLoggingIn = false
      if (isLogin === true) {
        window.localStorage.setItem('token', data.token)
        state.token = data.token
        state.data = data.user
        state.isLogin = true
      }
    },
    [loginAction.rejected]: (state, action) => {
      state.isLoggingIn = false
    },

    [logoutAction.fulfilled]: (state, action) => {
      window.localStorage.removeItem('token')
      state.isLogin = false
    },

    [meAction.pending]: (state, action) => {
      state.isLoggingIn = true
    },
    [meAction.fulfilled]: (state, action) => {
      const { status, data } = action.payload
      const isLogin = status === 'success'
      state.isLoggingIn = false
      state.isLogin = isLogin
      state.isLoginChecked = true
      if (isLogin) {
        state.data = data.user
      }
    },
    [meAction.rejected]: (state, action) => {
      state.isLoggingIn = false
      state.isLoginChecked = true
    },
  },
})

export default authSlice.reducer
