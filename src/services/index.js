import _ from 'lodash'

export const fetchApi = ({ url = null, method = 'GET', data = {}, headers = {} }) => {
  const apiUrl = `${process.env.REACT_APP_API_URL}/api/${url}`
  const options = {
    method,
    headers,
  }
  if (!_.isEmpty(data)) {
    headers['Content-Type'] = 'application/json'
    options.body = data
  }
  return fetch(apiUrl, options)
    .then(response => response.json())
    .catch(error => {
      console.log('fetch error', apiUrl, error)
      return error
    })
}
