import queryString from 'query-string'

import { fetchApi } from './index'

export const mapFieldProduct = item => {
  return {
    id: item?.id,
    name: item?.name,
    price: item?.price,
    createdAt: item?.created_at,
  }
}

export const apiProductList = async ({ page = 1, limit = 10 }) => {
  return fetchApi({ url: `products?${queryString.stringify({ page, limit })}` })
}

export const apiProductDetail = async id => {
  return fetchApi({ url: `products/${id}` })
}

export const apiProductCreate = async ({ name, price }) => {
  const token = window.localStorage.getItem('token')
  return fetchApi({
    url: `products`,
    method: 'POST',
    data: JSON.stringify({ name, price }),
    headers: { Authorization: `Bearer ${token}` },
  })
}

export const apiProductUpdate = async ({ id, name, price }) => {
  const token = window.localStorage.getItem('token')
  return fetchApi({
    url: `products/${id}`,
    method: 'PUT',
    data: JSON.stringify({ name, price }),
    headers: { Authorization: `Bearer ${token}` },
  })
}

export const apiProductDelete = async id => {
  const token = window.localStorage.getItem('token')
  return fetchApi({ url: `products/${id}`, method: 'DELETE', headers: { Authorization: `Bearer ${token}` } })
}
