import { fetchApi } from './index'

export const apiLogin = async ({ username, password }) => {
  return fetchApi({ url: `auth/login`, method: 'POST', data: JSON.stringify({ username, password }) })
}

export const apiMe = async (token = window.localStorage.getItem('token')) => {
  return fetchApi({ url: `auth/me`, headers: { Authorization: `Bearer ${token}` } })
}
