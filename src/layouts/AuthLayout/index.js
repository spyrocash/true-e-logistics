import React from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'

import LanguageSwitcher from 'components/LanguageSwitcher'

import './style.less'

function AuthLayout({ children }) {
  const isLogin = useSelector(state => state.auth.isLogin)
  if (isLogin) return <Redirect to="/" />
  return (
    <div className="auth-layout">
      <div className="langauge-switcher-container">
        <LanguageSwitcher />
      </div>
      {children}
    </div>
  )
}

export default AuthLayout
