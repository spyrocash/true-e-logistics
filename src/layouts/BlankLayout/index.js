import React from 'react'

import './style.less'

function BlankLayout({ children }) {
  return <div className="blank-layout">{children}</div>
}

export default BlankLayout
