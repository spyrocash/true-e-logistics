import React, { useState, useEffect } from 'react'
import { Row, Col, Layout, Menu, Space, Dropdown, Avatar, Grid, Drawer, Button } from 'antd'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  DownOutlined,
  UserOutlined,
  SettingOutlined,
  LogoutOutlined,
} from '@ant-design/icons'
import { Link, Redirect, useHistory } from 'react-router-dom'
import _ from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { logoutAction } from 'reducers/auth'

import LanguageSwitcher from 'components/LanguageSwitcher'

import logo from 'assets/images/logo.png'

import './style.less'

const { useBreakpoint } = Grid

const { Header, Sider, Content } = Layout
const { SubMenu } = Menu

const recursiveMenus = ({ routes, t }) => {
  return _.map(routes, route => {
    if (route.hideInMenu) return
    const name = t(route.name)
    if (!route.routes) {
      return (
        <Menu.Item key={route.path} icon={<route.icon />}>
          {name}
        </Menu.Item>
      )
    }
    return (
      <SubMenu key={route.path} icon={<route.icon />} title={name}>
        {recursiveMenus({ routes: route.routes, t })}
      </SubMenu>
    )
  })
}

const LeftMenu = ({ routes, collapsed }) => {
  let history = useHistory()
  const { t } = useTranslation()
  return (
    <Sider trigger={null} collapsible collapsed={collapsed} width={256}>
      <div className="logo">
        <Link to="/">
          <img src={logo} alt="logo" />
          {/* <h1>Buzzebees Backoffice</h1> */}
        </Link>
      </div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={routes[0].path}
        onClick={({ key }) => {
          history.push(key)
        }}
      >
        {recursiveMenus({ routes, t })}
      </Menu>
    </Sider>
  )
}

function BasicLayout(props) {
  const { children, routes } = props
  const [collapsed, setCollapsed] = useState(false)
  const [showLeftMenu, setShowLeftMenu] = useState(false)
  const { sm, md } = useBreakpoint()
  const isLogin = useSelector(state => state.auth.isLogin)
  const username = useSelector(state => state.auth.data.username)
  const dispatch = useDispatch()
  const { t } = useTranslation()

  useEffect(() => {
    setCollapsed(md ? false : true)
  }, [md])

  useEffect(() => {
    if (showLeftMenu && sm) setShowLeftMenu(false)
  }, [sm, showLeftMenu])

  if (!isLogin) return <Redirect to="/auth" />

  return (
    <div className="basic-layout">
      <Layout>
        {sm && <LeftMenu routes={routes} collapsed={collapsed} />}
        <Layout>
          <Header>
            <Row justify="space-between">
              <Col>
                {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                  className: 'trigger',
                  onClick: () => {
                    !sm ? setShowLeftMenu(!showLeftMenu) : setCollapsed(!collapsed)
                  },
                })}
              </Col>
              <Col>
                <Space size={0}>
                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item>
                          <UserOutlined />
                          <span>{t('Profile')}</span>
                        </Menu.Item>
                        <Menu.Item>
                          <SettingOutlined />
                          <span>{t('Setting')}</span>
                        </Menu.Item>
                        <Menu.Divider />
                        <Menu.Item onClick={() => dispatch(logoutAction())}>
                          <LogoutOutlined />
                          <span>{t('Logout')}</span>
                        </Menu.Item>
                      </Menu>
                    }
                  >
                    <Button className="user-dropdown" type="link">
                      <Avatar size="small" icon={<UserOutlined />} /> {username} <DownOutlined />
                    </Button>
                  </Dropdown>
                  <LanguageSwitcher />
                </Space>
              </Col>
            </Row>
          </Header>
          <Content>{children}</Content>
        </Layout>
      </Layout>
      <Drawer
        className="menu-left-drawer"
        placement="left"
        closable={false}
        onClose={() => setShowLeftMenu(false)}
        visible={showLeftMenu}
        bodyStyle={{ padding: 0 }}
      >
        <LeftMenu routes={routes} />
      </Drawer>
    </div>
  )
}

export default BasicLayout
