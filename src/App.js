import React, { Suspense, lazy, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { ConfigProvider } from 'antd'
import _ from 'lodash'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { Provider, useDispatch, useSelector } from 'react-redux'

import { meAction } from 'reducers/auth'

import routes from 'config/routes'
import langauges from 'config/langauges'

import store from 'reducers'

import 'locales'

import 'App.less'

const Loading = () => <div>Loading...</div>

const recursiveRoutes = routes => {
  return _.map(routes, (route, index) => {
    const Component = route.component ? lazy(() => import(`./${route.component}`)) : ({ children }) => children
    return (
      <Route key={`${route.path}-${index}`} path={route.path} exact={!route.routes}>
        <Component route={route} routes={route.routes}>
          {route.routes && (
            <Switch>
              {recursiveRoutes(route.routes)}
              <Redirect to={route.routes[0].path} />
            </Switch>
          )}
        </Component>
      </Route>
    )
  })
}

function Index() {
  const dispatch = useDispatch()

  const isLoginChecked = useSelector(state => state.auth.isLoginChecked)

  useEffect(() => {
    dispatch(meAction())
  }, [dispatch])

  if (!isLoginChecked) return <Loading />

  return (
    <Suspense fallback={<Loading />}>
      <Switch>{recursiveRoutes(routes)}</Switch>
    </Suspense>
  )
}

function App() {
  const { i18n } = useTranslation()

  const language = _.find(langauges, { key: i18n.language })

  useEffect(() => {
    moment.locale(language.momentLocale)
  }, [language])

  return (
    <Provider store={store}>
      <ConfigProvider locale={language.antdLocale}>
        <Router basename="/">
          <Index />
        </Router>
      </ConfigProvider>
    </Provider>
  )
}

export default App
