export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

export const randomBetween = (min, max) => Math.floor(Math.random() * max) + min
