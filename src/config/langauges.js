import enUS from 'antd/es/locale/en_US'
import thTH from 'antd/es/locale/th_TH'

import enTranslationDefault from 'locales/en/default.json'
import thTranslationDefault from 'locales/th/default.json'

import 'moment/locale/th'

export default [
  {
    key: 'en',
    flag: 'us',
    name: 'English',
    translation: enTranslationDefault,
    antdLocale: enUS,
    momentLocale: 'en',
  },
  {
    key: 'th',
    flag: 'th',
    name: 'Thai',
    translation: thTranslationDefault,
    antdLocale: thTH,
    momentLocale: 'th',
  },
]
