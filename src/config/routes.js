import { HomeOutlined } from '@ant-design/icons'

export default [
  {
    path: '/',
    component: 'layouts/BlankLayout',
    routes: [
      {
        path: '/auth',
        component: 'layouts/AuthLayout',
        routes: [
          {
            name: 'login',
            path: '/auth/login',
            component: 'pages/auth/login',
          },
          {
            name: 'register',
            path: '/auth/register',
            component: 'pages/auth/register',
          },
        ],
      },
      {
        path: '/',
        component: 'layouts/BasicLayout',
        routes: [
          {
            name: 'Products',
            icon: HomeOutlined,
            path: '/products',
            component: 'pages/product/list',
          },
          {
            hideInMenu: true,
            path: '/products/:id',
            component: 'pages/product/detail',
          },
        ],
      },
    ],
  },
]
