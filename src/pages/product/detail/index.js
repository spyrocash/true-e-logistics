import React, { useState, useEffect } from 'react'
import { PageHeader, Button, message, Form, Input, InputNumber, Card } from 'antd'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'

import { mapFieldProduct, apiProductCreate, apiProductDetail, apiProductUpdate } from 'services/product'

import './style.less'

function ProductDetailPage() {
  let history = useHistory()
  let { id } = useParams()
  const { t } = useTranslation()
  const [form] = Form.useForm()
  const [loading, setLoading] = useState(false)
  const [submitting, setSubmitting] = useState(false)

  const isCreate = id === 'add'

  useEffect(() => {
    if (isCreate) return

    const init = async () => {
      setLoading(true)

      const productDetailResult = await apiProductDetail(id)

      setLoading(false)

      if (_.isEmpty(productDetailResult.data)) {
        history.push('/products')
        return
      }

      const item = mapFieldProduct(productDetailResult.data)
      form.setFieldsValue(item)
    }

    init()
  }, [isCreate, id, form, history])

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  }

  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  }

  const onSubmit = async ({ name, price }) => {
    if (submitting) return
    setSubmitting(true)
    if (isCreate) {
      const productCreateResult = await apiProductCreate({ name, price })
      setSubmitting(false)
      if (productCreateResult.status === 'error') {
        message.error(productCreateResult.error_message)
        return
      }
      message.success(t('Create Success'))
      history.push('/products')
    } else {
      const productUpdateResult = await apiProductUpdate({ id, name, price })
      setSubmitting(false)
      if (productUpdateResult.status === 'error') {
        message.error(productUpdateResult.error_message)
        return
      }
      message.success(t('Update Success'))
      history.push('/products')
    }
  }

  const title = `Product ${isCreate ? 'Add' : 'Update'}`

  return (
    <div className="product-detail-page">
      <PageHeader
        title={t(title)}
        ghost={false}
        onBack={() => {
          history.length > 0 ? history.goBack() : history.push('/products')
        }}
      />
      <Card loading={loading}>
        <Form {...layout} form={form} onFinish={onSubmit}>
          <Form.Item
            label={t('Product Name')}
            name="name"
            rules={[{ required: true, message: t('Please input product name!') }]}
          >
            <Input autoFocus />
          </Form.Item>
          <Form.Item
            label={t('Product Price')}
            name="price"
            rules={[{ required: true, message: t('Please input product price!') }]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" loading={submitting}>
              {t('Submit')}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}

export default ProductDetailPage
