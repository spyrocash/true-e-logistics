import React, { useState, useEffect } from 'react'
import { PageHeader, Button, Table, Tooltip, Space, Popconfirm, message } from 'antd'
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import queryString from 'query-string'
import moment from 'moment'
import _ from 'lodash'

import { mapFieldProduct, apiProductList, apiProductDelete } from 'services/product'

import './style.less'

function ProductListPage() {
  let history = useHistory()
  useRouteMatch()
  const { t } = useTranslation()
  const [loading, setLoading] = useState(false)
  const [items, setItems] = useState([])
  const [itemsCount, setItemsCount] = useState(null)

  let query = queryString.parse(window.location.search)
  const page = _.toInteger(query.page) || 1
  const limit = _.toInteger(query.limit) || 10

  useEffect(() => {
    const init = async () => {
      setLoading(true)
      const productListResult = await apiProductList({ page, limit })
      setLoading(false)
      setItems(_.map(productListResult.data, item => mapFieldProduct(item)))
      setItemsCount(productListResult.total)
    }

    init()
  }, [page, limit])

  const columns = [
    {
      title: t('No.'),
      key: 'no',
      align: 'center',
      render: (text, record, index) => (page - 1) * limit + (index + 1),
    },
    {
      title: t('Product Name'),
      key: 'name',
      dataIndex: 'name',
      align: 'center',
    },
    {
      title: t('Product Price'),
      key: 'price',
      dataIndex: 'price',
      align: 'center',
    },
    {
      title: t('Create Date'),
      key: 'createdAt',
      dataIndex: 'createdAt',
      align: 'center',
      render: text => moment(text).format('DD MMM YYYY HH:mm'),
    },
    {
      title: t('Action'),
      key: 'action',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Tooltip title={t('Edit')}>
            <Button
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                history.push(`/products/${record.id}`)
              }}
            />
          </Tooltip>
          <Tooltip title={t('Delete')}>
            <Popconfirm
              placement="bottom"
              title={t('Are you sure delete this task?')}
              onConfirm={async () => {
                if (loading) return
                setLoading(true)
                const deleteProductResult = await apiProductDelete(record.id)
                if (deleteProductResult.status === 'error') {
                  message.error(deleteProductResult.error_message)
                } else {
                  message.success(t('Delete Success'))
                  const productListResult = await apiProductList({ page, limit })
                  setItems(_.map(productListResult.data, item => mapFieldProduct(item)))
                  setItemsCount(productListResult.total)
                }
                setLoading(false)
              }}
              okText={t('Yes')}
              cancelText={t('No')}
              okButtonProps={{
                shape: 'round',
              }}
              cancelButtonProps={{
                shape: 'round',
              }}
            >
              <Button shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ]

  const dataSource = _.map(items, item => {
    return {
      key: item.id,
      ...item,
    }
  })

  return (
    <div className="product-list-page">
      <PageHeader
        title={t('Products')}
        ghost={false}
        extra={[
          <Button
            key="add-product"
            type="primary"
            ghost
            icon={<PlusOutlined />}
            shape="round"
            onClick={() => {
              history.push('/products/add')
            }}
          >
            {t('Add Product')}
          </Button>,
        ]}
      />
      <Table
        columns={columns}
        dataSource={dataSource}
        size="small"
        loading={loading}
        pagination={{
          current: page,
          pageSize: limit,
          total: itemsCount,
          showQuickJumper: true,
          showSizeChanger: true,
          showTotal: total => `${t('Total')} ${total} ${t('items')}`,
        }}
        onChange={pagination => {
          history.push({
            pathname: '/products',
            search: `?${queryString.stringify({ page: pagination.current, limit: pagination.pageSize })}`,
          })
        }}
      />
    </div>
  )
}

export default ProductListPage
