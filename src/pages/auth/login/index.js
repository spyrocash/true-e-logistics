import React from 'react'
import { Form, Input, Button, message, Alert } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'

import BackgroundImage from 'components/BackgroundImage'

import { loginAction } from 'reducers/auth'

import logo from 'assets/images/logo.png'
import bg1 from 'assets/images/bg-1.png'
import bg2 from 'assets/images/bg-2.png'

import './style.less'

function LoginPage() {
  const { t } = useTranslation()
  const isLoggingIn = useSelector(state => state.auth.isLoggingIn)
  const dispatch = useDispatch()

  const submitLogin = async ({ username, password }) => {
    const loginResult = await dispatch(loginAction({ username, password }))
    if (loginResult.payload?.status === 'error') {
      message.error(loginResult.payload?.errorMessage)
    }
  }

  return (
    <div className="login-page">
      <div className="left">
        <div className="background-block">
          <BackgroundImage image={bg1} />
        </div>
        <div className="content-block">
          <div className="logo-block">
            <img src={logo} alt="" />
          </div>
        </div>
      </div>
      <div className="right">
        <div className="background-block">
          <BackgroundImage image={bg2} />
        </div>
        <div className="content-block">
          <div className="logo-block">
            <img src={logo} alt="" />
          </div>
          <div className="login-form-block">
            <h2>{t('Login')}</h2>
            <Alert
              message={
                <div>
                  <div>Username: admin</div>
                  <div>Password: admin</div>
                </div>
              }
              type="warning"
            />
            <Form
              hideRequiredMark
              layout="vertical"
              name="login"
              className="login-form"
              initialValues={{ remember: true }}
              onFinish={({ username, password }) => submitLogin({ username, password })}
            >
              <Form.Item
                label={t('Username')}
                name="username"
                rules={[{ required: true, message: t('Please input your username!') }]}
              >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder={t('Username')} />
              </Form.Item>
              <Form.Item
                label={t('Password')}
                name="password"
                rules={[{ required: true, message: t('Please input your password!') }]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder={t('Password')}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  danger
                  block
                  htmlType="submit"
                  className="login-form-button"
                  loading={isLoggingIn}
                >
                  {t('Log in')}
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LoginPage
