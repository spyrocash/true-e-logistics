import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
import _ from 'lodash'

import langauges from 'config/langauges'
import setting from 'config/setting'

const { defaultLanguage } = setting

const resources = _.reduce(
  langauges,
  (result, value) => {
    return {
      ...result,
      [value.key]: {
        translation: value.translation,
      },
    }
  },
  {}
)

i18n
  .use(LanguageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    // we init with resources
    resources,
    fallbackLng: defaultLanguage,
    // debug: process.env.NODE_ENV !== 'production',
    debug: false,

    keySeparator: false, // we use content as keys

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  })

export default i18n
