import React from 'react'
import { Menu, Dropdown, Button } from 'antd'
import { GlobalOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'

import langauges from 'config/langauges'

function LanguageSwitcher() {
  const { i18n } = useTranslation()

  const changeLanguage = lng => i18n.changeLanguage(lng)

  return (
    <Dropdown
      overlay={
        <Menu selectedKeys={i18n.language}>
          {_.map(langauges, langauge => {
            const { key, flag, name } = langauge
            return (
              <Menu.Item key={key} onClick={() => changeLanguage(key)}>
                <span className="anticon">
                  <span className={`flag-icon flag-icon-${flag}`}></span>
                </span>
                <span>{name}</span>
              </Menu.Item>
            )
          })}
        </Menu>
      }
    >
      <Button type="link">
        <GlobalOutlined style={{ fontSize: 16 }} />
      </Button>
    </Dropdown>
  )
}

export default LanguageSwitcher
