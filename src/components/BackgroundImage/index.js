import React from 'react'
import styled from 'styled-components'

function BackgroundImage({ className, image, position, repeat, size }) {
  return (
    <Style
      className={`background-image-component ${className}`}
      image={image}
      position={position}
      repeat={repeat}
      size={size}
    />
  )
}

export default BackgroundImage

BackgroundImage.defaultProps = {
  className: '',
  image: null,
  position: 'center',
  repeat: 'no-repeat',
  size: 'cover',
}

const Style = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${props => props.image});
  background-position: ${props => props.position};
  background-repeat: ${props => props.repeat};
  background-size: ${props => props.size};
`
