const CracoLessPlugin = require('craco-less')
const modifyVars = require('./src/config/modifyVars')

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars,
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
}
